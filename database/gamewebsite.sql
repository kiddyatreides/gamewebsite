-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 07, 2017 at 07:41 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gamewebsite`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE IF NOT EXISTS `tb_admin` (
  `id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `fName` varchar(200) NOT NULL,
  `lName` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`id`, `username`, `email`, `password`, `fName`, `lName`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'fansi@gmail.com', 'eyJpdiI6IkhhS3JRU3BIWHBNRE1DVnFhbTUwT1E9PSIsInZhbHVlIjoiempwN2M5ZTJQYnpRN0dsS0pcL3BQK3c9PSIsIm1hYyI6IjQzOGNlMTY3MjcxOWE1OGQwZWFiN2NlOTQ4Y2QzNzQ3YzUyNmM5NWZkZTVmMmFjYjA1YWY1MzI2NzcyN2IxODAifQ==', 'Fansi', 'Fansi', '2017-04-22 03:29:08', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_age`
--

CREATE TABLE IF NOT EXISTS `tb_age` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `picture` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_age`
--

INSERT INTO `tb_age` (`id`, `name`, `description`, `picture`, `created_at`, `updated_at`) VALUES
(1, 'Age 5', '3 - 10 Tahun', '14929134185.png', '2017-04-22 03:29:52', '2017-04-22 19:39:58'),
(2, 'Age 9', 'test', '14929133789.png', '2017-04-22 19:05:22', '2017-04-22 19:39:43');

-- --------------------------------------------------------

--
-- Table structure for table `tb_comment`
--

CREATE TABLE IF NOT EXISTS `tb_comment` (
  `id` int(11) NOT NULL,
  `id_game` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `comment` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_developer`
--

CREATE TABLE IF NOT EXISTS `tb_developer` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_developer`
--

INSERT INTO `tb_developer` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'electroarts', 'HAHAHAtes', '2017-04-22 03:31:21', '2017-04-21 21:14:11'),
(2, 'origin', 'asdasda', '2017-04-22 03:31:21', '2017-04-22 04:11:04');

-- --------------------------------------------------------

--
-- Table structure for table `tb_game`
--

CREATE TABLE IF NOT EXISTS `tb_game` (
  `id` int(11) NOT NULL,
  `game_title` varchar(200) NOT NULL,
  `id_genre` int(200) NOT NULL,
  `game_description` text NOT NULL,
  `game_picture` text NOT NULL,
  `id_rating` int(11) NOT NULL,
  `id_platform` int(11) NOT NULL,
  `id_admin` int(11) NOT NULL,
  `game_price` int(11) NOT NULL,
  `game_release` date NOT NULL,
  `game_access` text NOT NULL,
  `id_developer` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_game`
--

INSERT INTO `tb_game` (`id`, `game_title`, `id_genre`, `game_description`, `game_picture`, `id_rating`, `id_platform`, `id_admin`, `game_price`, `game_release`, `game_access`, `id_developer`, `created_at`, `updated_at`) VALUES
(1, 'flappy bird', 1, '<p><em><strong>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. </strong></em></p>\r\n<p>&nbsp;</p>\r\n<p><em><strong>Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,</strong></em></p>', 'flappybird.jpg', 2, 1, 1, 50000, '2017-04-18', 'http://facebook.com', 1, '2017-04-15 01:26:49', '2017-04-28 21:52:23'),
(2, 'Mario Bros', 2, 'abcd', '1', 1, 2, 1, 10000, '2017-04-22', 'facebook', 2, '2017-04-14 20:12:20', '2017-04-23 03:59:33'),
(3, 'sims 4', 3, 'game seru', '1492314773Brow Powder.PNG', 1, 3, 1, 150000, '2017-04-22', 'origin', 2, '2017-04-15 20:44:55', '2017-04-23 03:59:38'),
(4, 'sad', 1, '<p>asd</p>', '1494093691146665.jpg', 1, 1, 1, 1231231, '2017-08-09', '#', 1, '2017-05-06 11:01:31', '2017-05-06 11:01:31');

-- --------------------------------------------------------

--
-- Table structure for table `tb_genre`
--

CREATE TABLE IF NOT EXISTS `tb_genre` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_genre`
--

INSERT INTO `tb_genre` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Action', 'Action', '2017-04-22 03:33:58', '2017-04-22 03:33:58'),
(2, 'RPG', 'RPG', '2017-04-22 19:33:55', '2017-04-22 19:33:55'),
(3, 'Card', 'Card', '2017-04-22 19:34:07', '2017-04-22 19:34:07');

-- --------------------------------------------------------

--
-- Table structure for table `tb_platform`
--

CREATE TABLE IF NOT EXISTS `tb_platform` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_platform`
--

INSERT INTO `tb_platform` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Android', 'Android', '2017-04-22 03:36:20', '2017-04-22 03:36:20'),
(2, 'iOS', 'iOS', '2017-04-22 18:53:28', '2017-04-22 18:53:28'),
(3, 'PC', 'PC', '2017-04-22 18:53:39', '2017-04-22 18:53:39'),
(4, 'Android, Web Browser, iOS', '<p>sdfsdf</p>', '2017-05-06 20:33:07', '2017-05-06 20:33:07');

-- --------------------------------------------------------

--
-- Table structure for table `tb_rating`
--

CREATE TABLE IF NOT EXISTS `tb_rating` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `picture` text NOT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_rating`
--

INSERT INTO `tb_rating` (`id`, `name`, `picture`, `description`, `created_at`, `updated_at`) VALUES
(1, '3', '3.png', 'Rating 3', '2017-04-22 03:37:14', '2017-04-22 03:37:14'),
(2, '3', '3.png', 'Suitable for age 3 and above', '2017-04-22 03:37:14', '2017-04-22 03:37:14');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE IF NOT EXISTS `tb_user` (
  `id` int(11) NOT NULL,
  `gender` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` text NOT NULL,
  `id_age` int(11) DEFAULT NULL,
  `fName` varchar(200) NOT NULL,
  `lName` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `gender`, `email`, `password`, `id_age`, `fName`, `lName`, `created_at`, `updated_at`) VALUES
(1, 'women', 'fansi.lantana@yahoo.com', '12345', NULL, 'Fansi', 'Lantana', '2017-04-08 20:07:01', '2017-04-08 20:07:01'),
(2, 'women', 'fansi.lantana@yahoo.com', 'eyJpdiI6ImtjQU9LRVJmcUtPXC9BY0FPQTdxMXlRPT0iLCJ2YWx1ZSI6InlhU1NZbFI0akI2dlRoVHVVMkNRZnc9PSIsIm1hYyI6IjFjOGM1MWJiNmQzZDg0NjFhY2E1MTNlMDZkN2Q3YjZjMzhhNjhkZTIxM2YyNGJmOGFkZWFhNGZhNWVmODc0MDMifQ==', NULL, 'Fansi', 'Lantana', '2017-04-08 20:08:11', '2017-04-08 20:08:11'),
(3, 'women', 'fansi.lantana@yahoo.com', 'eyJpdiI6InhLdFR1WHY1QVwvK2F2N0tTSm9VSUxnPT0iLCJ2YWx1ZSI6InpSZW9DVHBWZWJQSnRFckxOKzc3Z0E9PSIsIm1hYyI6ImM4NzA5MDlkMGJhZWE5ZDdlMTBmMGFmN2NmODRkYjMwODQ2MWY5NGEyZjc3ZTk3NDM1MTI5MmU0MGUyOTc0MjgifQ==', NULL, 'Fansi', 'Lantana', '2017-04-15 04:22:03', '2017-04-15 04:22:03'),
(4, 'women', 'fansi.lantana@gmail.com', 'eyJpdiI6IkhhS3JRU3BIWHBNRE1DVnFhbTUwT1E9PSIsInZhbHVlIjoiempwN2M5ZTJQYnpRN0dsS0pcL3BQK3c9PSIsIm1hYyI6IjQzOGNlMTY3MjcxOWE1OGQwZWFiN2NlOTQ4Y2QzNzQ3YzUyNmM5NWZkZTVmMmFjYjA1YWY1MzI2NzcyN2IxODAifQ==', NULL, 'Fansi', 'Lantana', '2017-04-15 19:21:37', '2017-04-15 19:21:37'),
(5, 'Male', 'hudya@gmail.com', 'eyJpdiI6InR6XC90SVY2a0gxN2w5UlFwXC96TW44QT09IiwidmFsdWUiOiIzK1I0VEs0aHNCVFQxd0pnK1lGMmRRPT0iLCJtYWMiOiI0ZjY2OTVmZDEyYWMxYzZiMTQyMjc1Yzc0N2JmZDY0NGYxMmVlMTE1OThkZjE1ZTg3YjM2ODkxYzEzMGNmYzJkIn0=', NULL, 'Muhamadh', 'Hudya', '2017-04-22 16:52:41', '2017-04-22 16:52:41'),
(6, '-- Gender --', 'hudyaa@gmail.com', 'eyJpdiI6Ik1oOXFqczl0bHd6bDRRcVNYNHBmMFE9PSIsInZhbHVlIjoiWWh3TzUrYnpCbVllZFVOU3VzXC9NZ3c9PSIsIm1hYyI6ImIyMmVlNTcwMTIyODRjMWE1NDU2ZjRjZTU1MmRjNGNiNmUyZWJhYzcxNzFhNThlNDI3NTQ0NzRlNmM2OTAwMzUifQ==', NULL, 'Muhamad', 'Hudya', '2017-04-28 07:46:49', '2017-04-28 07:46:49');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_rating`
--

CREATE TABLE IF NOT EXISTS `tb_user_rating` (
  `id` int(11) NOT NULL,
  `id_game` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `violence` int(11) NOT NULL,
  `language` int(11) NOT NULL,
  `fear_horror` int(11) NOT NULL,
  `online_interaction` int(11) NOT NULL,
  `total` int(11) DEFAULT NULL,
  `average` float DEFAULT NULL,
  `review` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user_rating`
--

INSERT INTO `tb_user_rating` (`id`, `id_game`, `id_user`, `violence`, `language`, `fear_horror`, `online_interaction`, `total`, `average`, `review`, `created_at`, `updated_at`) VALUES
(12, 1, 4, 2, 2, 2, 2, 8, 2.66667, 'bagus', '2017-04-22 16:51:56', '2017-04-22 16:51:56'),
(13, 1, 5, 1, 1, 1, 1, 4, 1.33333, 'lumayan lah', '2017-04-22 16:53:30', '2017-04-22 16:53:30'),
(14, 1, 6, 0, 0, 1, 2, 3, 1, 'testtt', '2017-04-28 07:47:58', '2017-04-28 07:47:58'),
(16, 3, 6, 2, 2, 2, 2, 8, 2.66667, 'bagus tapi gimana ya', '2017-04-28 17:18:36', '2017-04-28 17:18:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_age`
--
ALTER TABLE `tb_age`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_comment`
--
ALTER TABLE `tb_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_developer`
--
ALTER TABLE `tb_developer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_game`
--
ALTER TABLE `tb_game`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_genre`
--
ALTER TABLE `tb_genre`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_platform`
--
ALTER TABLE `tb_platform`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_rating`
--
ALTER TABLE `tb_rating`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user_rating`
--
ALTER TABLE `tb_user_rating`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_age`
--
ALTER TABLE `tb_age`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_comment`
--
ALTER TABLE `tb_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_developer`
--
ALTER TABLE `tb_developer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_game`
--
ALTER TABLE `tb_game`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_genre`
--
ALTER TABLE `tb_genre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_platform`
--
ALTER TABLE `tb_platform`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_rating`
--
ALTER TABLE `tb_rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_user_rating`
--
ALTER TABLE `tb_user_rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
