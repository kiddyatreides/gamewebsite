<?php

namespace App\Http\Controllers;

use App\MOadminsignup;
use Illuminate\Http\Request;

class COadminsignin extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('backend.adminsignin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $email = $request->email;
        $password = $request->password;

        $result = false;

        $checkLogin = new MOadminsignup();

        $data = $checkLogin->all();

        foreach($data as $logindata){
            if($email == $logindata->email && $password == decrypt($logindata->password)){
                $result = true;
                $request->session()->put('nameadmin',$logindata->lName);
                $request->session()->put('tokenadmin',csrf_token());
            }
        }

        if($result){
            return redirect()->route('admingamelist.index');
        }
        else{
            echo "Login Gagal\n";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
