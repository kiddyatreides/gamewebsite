<?php

namespace App\Http\Controllers;

use App\MOGame;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class COadmingame extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (!session()->has('tokenadmin')) {
            return redirect()->route('adminsignin.index');
        }
        else {
            $game = DB::table('tb_game')
                ->join('tb_genre', 'tb_game.id_genre', '=', 'tb_genre.id')
                ->join('tb_developer', 'tb_game.id_developer', '=', 'tb_developer.id')
                ->select('tb_game.*', 'tb_genre.name as nama_genre', 'tb_developer.name as nama_developer')
                ->get();
            return view('backend.admingamelist', compact('game'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $age = DB::table('tb_age')->get();
        $genre = DB::table('tb_genre')->get();
        $platform = DB::table('tb_platform')->get();
        $developer = DB::table('tb_developer')->get();


        return view('backend.addgame', compact('age','genre','platform','developer'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $cruds = new MOGame();
        $cruds->game_title = $request->game_title;
        $cruds->game_description = $request->description;
        $cruds->id_rating = $request->rating;
        $cruds->id_platform = $request->platform;
        $cruds->id_admin = 1;
        $cruds->id_genre = $request->genre;
        $cruds->id_developer = $request->developer;
        $cruds->game_price = $request->price;
        $cruds->game_release = $request->release;
        $cruds->game_access = $request->access;


        $file = $request->file('gambar'); //request file gambar

        $fileName = $file->getClientOriginalName(); //ambil nama asli gambar
        $unique_name = time().$fileName; //make new name biar ngga bentrok kalau nama sama
        $request->file('gambar')->move("upload/game/",$unique_name); // dari yang request di pindahin folder upload game

        $cruds->game_picture = $unique_name; //game picture di db di isi dengan unique name

        $cruds->save();
        return redirect()->route('admingamelist.index')->with('alert-success', 'Data Berhasil Disimpan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $game = DB::table('tb_game')
            ->join('tb_genre', 'tb_game.id_genre', '=', 'tb_genre.id')
            ->join('tb_admin', 'tb_game.id_admin', '=', 'tb_admin.id')
            ->join('tb_platform', 'tb_game.id_platform', '=', 'tb_platform.id')
            ->join('tb_age', 'tb_game.id_rating', '=', 'tb_age.id')
            ->join('tb_developer', 'tb_game.id_developer', '=', 'tb_developer.id')
            ->select('tb_game.*', 'tb_admin.fName as nama_admin',
                'tb_genre.name as nama_genre', 'tb_age.name as rating','tb_platform.name as platform'
                ,'tb_developer.name as developer')
            ->where('tb_game.id', $id)->first();

        $rating = DB::table('tb_age')->get();
        $genre = DB::table('tb_genre')->get();
        $platform = DB::table('tb_platform')->get();
        $developer = DB::table('tb_developer')->get();
        return view('backend.editgame', compact('game','rating','genre','platform','developer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $cruds = MOGame::findOrFail($id);
        $cruds->game_title = $request->game_title;
        $cruds->game_description = $request->description;
        $cruds->id_rating = $request->rating;
        $cruds->id_platform = $request->platform;
        $cruds->id_genre = $request->genre;
        $cruds->id_developer = $request->developer;
        $cruds->game_price = $request->price;
        $cruds->game_release = $request->release;
        $cruds->game_access = $request->access;


        //gambar
        if($request->file('gambar') == "") //kalau gambar tidak ada
        {
            $cruds->game_picture = $cruds->game_picture; //tetap gambar & nama yang sama
        }
        else
        {
        $file = $request->file('gambar');
            $fileName = $file->getClientOriginalName();
            $unique_name = time().$fileName;
            $request->file('gambar')->move("upload/game/",$unique_name);

            $cruds->game_picture = $unique_name;
        }

        $cruds->save();
        return redirect()->route('admingamelist.index')->with('alert-success', 'Data Berhasil Diubah.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $cruds = MOGame::findOrFail($id);
        $cruds->delete();
        return redirect()->route('admingamelist.index')->with('alert-success', 'Data Berhasil Dihapus.');
    }
}
