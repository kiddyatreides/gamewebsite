<?php

namespace App\Http\Controllers;

use App\MOplatform;
use Illuminate\Http\Request;

class COadminplatform extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $platform = MOplatform::all();
        return view('backend.listplatform', compact('platform'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('backend.addplatform');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $developer = new MOplatform();
        $developer->name = $request->name;
        $developer->description = $request->description;
        $developer->save();

        return redirect()->route('adminplatform.index')->with('alert-success', 'Data Berhasil Disimpan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $platform = MOplatform::findOrFail($id);
        return view('backend.editplatform', compact('platform'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $developer = MOplatform::findOrFail($id);
        $developer->name = $request->name;
        $developer->description = $request->description;
        $developer->save();
        return redirect()->route('adminplatform.index')->with('alert-success', 'Data Berhasil Diubah.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $developer = MOplatform::findOrFail($id);
        $developer->delete();
        return redirect()->route('adminplatform.index')->with('alert-success', 'Data Berhasil Dihapus.');
    }
}
