<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;


class COhome extends Controller
{
    //

    public function index(){

        //VARIABLE YANG HARUS SELALU DIBAWA DI FRONTEND//
        $title = 'My Title Here';
        $age = DB::table('tb_age')->get();
        $genre = DB::table('tb_genre')->get();
        $platform = DB::table('tb_platform')->get();

        View::share(array(
            'title' => $title,
            'age' => $age,
            'genre' => $genre,
            'platform' => $platform
        ));
        //END VARIABLE

        return view('frontend.home',compact('age')); //menampilkan frontend home
    }

}
