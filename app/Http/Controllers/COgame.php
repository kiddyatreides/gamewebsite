<?php

namespace App\Http\Controllers;

use App\MOGame;
use App\MOratinggame;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class COgame extends Controller
{

    public function index(){
        //VARIABLE YANG HARUS SELALU DIBAWA DI FRONTEND//
        $title = 'My Title Here';
        $age = DB::table('tb_age')->get();
        $genre = DB::table('tb_genre')->get();
        $platform = DB::table('tb_platform')->get();

        View::share(array(
            'title' => $title,
            'age' => $age,
            'genre' => $genre,
            'platform' => $platform
        ));
        //END VARIABLE

        $game = DB::table('tb_game')
            ->join('tb_genre','tb_game.id_genre', '=', 'tb_genre.id') //manggil return view game list di bawa $dgame untuk di panggil
            ->join('tb_admin','tb_game.id_admin', '=', 'tb_admin.id')
            ->join('tb_platform','tb_game.id_platform', '=', 'tb_platform.id')
            ->join('tb_age','tb_game.id_rating', '=', 'tb_age.id')
            ->join('tb_developer','tb_game.id_developer', '=', 'tb_developer.id')
            ->select('tb_game.*','tb_admin.fName as name_admin', 'tb_genre.name as genre', 'tb_age.picture as rating'
                , 'tb_platform.name as platform','tb_developer.name as developer') //jadiin alias untuk di panggil ke view
            ->simplePaginate(5);

        $rating2 = DB::table('tb_user_rating')
            ->join('tb_game', 'tb_user_rating.id_game', '=','tb_game.id')
            ->select(DB::raw('AVG( tb_user_rating.average ) as rata'), DB::raw('COUNT( tb_user_rating.average ) as jumlah')
                , DB::raw('AVG( tb_user_rating.violence ) as viol'), DB::raw('AVG( tb_user_rating.language ) as lang')
                , DB::raw('AVG( tb_user_rating.fear_horror ) as fh'), DB::raw('AVG( tb_user_rating.online_interaction ) as oi')
                , DB::raw('AVG( tb_user_rating.average ) as avgg'))
            ->where('tb_user_rating.id_game', 1)
            ->get();

        $low = DB::table('tb_age')
            ->select('tb_age.*')
            ->where('tb_age.id',1)
            ->get();

        $med = DB::table('tb_age')
            ->select('tb_age.*')
            ->where('tb_age.id',2)
            ->get();



        $data = array(
          'game' => $game,
          'rating' => $rating2,
             'agelow' => $low,
            'agemed' => $med,
        );

        return view('frontend.gamelist')->with($data);
    }

    public function show($id) //game/3
    {
        //VARIABLE YANG HARUS SELALU DIBAWA DI FRONTEND//
        $title = 'My Title Here';
        $age = DB::table('tb_age')->get();
        $genre = DB::table('tb_genre')->get();
        $platform = DB::table('tb_platform')->get();

        View::share(array(
            'title' => $title,
            'age' => $age,
            'genre' => $genre,
            'platform' => $platform
        ));
        //END VARIABLE

        $game = DB::table('tb_game')
            ->join('tb_genre', 'tb_game.id_genre', '=', 'tb_genre.id')
            ->join('tb_admin', 'tb_game.id_admin', '=', 'tb_admin.id')
            ->join('tb_platform', 'tb_game.id_platform', '=', 'tb_platform.id')
            ->join('tb_age', 'tb_game.id_rating', '=', 'tb_age.id')
            ->join('tb_developer', 'tb_game.id_developer', '=', 'tb_developer.id')
            ->select('tb_game.*', 'tb_genre.name as genre', 'tb_platform.name as platform', 'tb_developer.name as developer','tb_age.picture')
            ->where('tb_game.id', $id)
            ->get();


        $low = DB::table('tb_age')
            ->select('tb_age.*')
            ->where('tb_age.id',1)
            ->get();

        $med = DB::table('tb_age')
            ->select('tb_age.*')
            ->where('tb_age.id',2)
            ->get();

        $session = session()->get('id_user');

        $user = DB::table('tb_user_rating')
            ->select(DB::raw('count(tb_user_rating.id_user) as user'))
            ->where('tb_user_rating.id_game', $id)
            ->where('tb_user_rating.id_user',$session)
            ->get();

        $rating2 = DB::table('tb_user_rating')
            ->select(DB::raw('AVG( tb_user_rating.average ) as rata'), DB::raw('COUNT( tb_user_rating.average ) as jumlah')
                , DB::raw('AVG( tb_user_rating.violence ) as viol'), DB::raw('AVG( tb_user_rating.language ) as lang')
                , DB::raw('AVG( tb_user_rating.fear_horror ) as fh'), DB::raw('AVG( tb_user_rating.online_interaction ) as oi')
                , DB::raw('AVG( tb_user_rating.average ) as avgg'))
            ->where('tb_user_rating.id_game', $id)
            ->get();

        $review = DB::table('tb_user_rating')
            ->join('tb_user', 'tb_user_rating.id_user', '=', 'tb_user.id')
            ->select('tb_user_rating.*','tb_user.fName','tb_user.lName')
            ->where('tb_user_rating.id_game', $id)
            ->get();

        $data = array(
            'game' => $game,
            'user' => $user,
            'rating' => $rating2,
            'agelow' => $low,
            'agemed' => $med,
            'review' => $review
        );

        return view('frontend.detailgame')->with($data);
    }

    public function store(Request $request){

        if (!session()->has('id_user')) {
            return redirect()->route('signin.index');
        }
        else {
            $x = new MOratinggame();

            $violence = $request->violence;
            $lang = $request->lang;
            $fh = $request->fh;
            $oi = $request->oi;

            $idgame = $request->id;
            $x->id_game = $request->id;
            $x->id_user = session()->get('id_user');
            $x->violence = $request->violence;
            $x->language = $request->lang;
            $x->fear_horror = $request->fh;
            $x->online_interaction = $request->oi;

            $total = ($violence + $lang + $fh + $oi);
            $x->total = $total;
            $x->average = ($total/3);
            $x->review = $request->review;
            $x->save();
            return redirect('/game/'.$idgame)->with('alert-success', 'Sukses Review.');
        }
    }
}
