<?php

namespace App\Http\Controllers;

use App\MOGame;
use App\MOgenre;
use Illuminate\Http\Request;

class COadmingenre extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $genre = MOgenre::all();
        return view('backend.listgenre', compact('genre'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('backend.addgenre');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $developer = new MOgenre();
        $developer->name = $request->name;
        $developer->description = $request->description;
        $developer->save();

        return redirect()->route('admingenre.index')->with('alert-success', 'Data Berhasil Disimpan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $genre = MOgenre::findOrFail($id);
        return view('backend.editgenre', compact('genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $developer = MOgenre::findOrFail($id);
        $developer->name = $request->name;
        $developer->description = $request->description;
        $developer->save();
        return redirect()->route('admingenre.index')->with('alert-success', 'Data Berhasil Diubah.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $developer = MOgenre::findOrFail($id);
        $developer->delete();
        return redirect()->route('admingenre.index')->with('alert-success', 'Data Berhasil Dihapus.');
    }
}
