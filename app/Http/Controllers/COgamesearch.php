<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class COgamesearch extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //VARIABLE YANG HARUS SELALU DIBAWA DI FRONTEND//
        $title = 'My Title Here';
        $age = DB::table('tb_age')->get();
        $genre = DB::table('tb_genre')->get();
        $platform = DB::table('tb_platform')->get();

        View::share(array(
            'title' => $title,
            'age' => $age,
            'genre' => $genre,
            'platform' => $platform
        ));
        //END VARIABLE

        //
        $game = $request->name;
        $genre = $request->genre;
        $age =  $request->age;
        $platform = $request->platform;

        $games = DB::table('tb_game')
            ->join('tb_genre','tb_game.id_genre', '=', 'tb_genre.id') //manggil return view game list di bawa $dgame untuk di panggil
            ->join('tb_admin','tb_game.id_admin', '=', 'tb_admin.id')
            ->join('tb_platform','tb_game.id_platform', '=', 'tb_platform.id')
            ->join('tb_age','tb_game.id_rating', '=', 'tb_age.id')
            ->select('tb_game.*','tb_admin.fName as name_admin', 'tb_genre.name as genre',
                'tb_age.picture as rating', 'tb_platform.name as platform') //jadiin alias untuk di panggil ke view
            ->where('tb_game.game_title', '=',$game)
            ->orWhere('tb_game.id_rating', '=',$age)
            ->orWhere('tb_game.id_genre', '=', $genre)
            ->orWhere('tb_game.id_platform', '=', $platform)
        ->get();

        return view('frontend.searchgame',compact('games'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
