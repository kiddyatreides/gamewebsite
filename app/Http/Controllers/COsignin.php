<?php

namespace App\Http\Controllers;

use App\MOsignup;
use Illuminate\Http\Request;

class COsignin extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('frontend.signin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $email = $request->email;
        $password = $request->password;

        $result = false;

        $checkLogin = new MOsignup();

        $data = $checkLogin->all();

        foreach($data as $logindata){
            if($email == $logindata->email && $password == decrypt($logindata->password)){
                $result = true;
                $request->session()->put('name',$logindata->lName);
                $request->session()->put('id_user',$logindata->id);
                $request->session()->put('token',csrf_token());
            }
        }

        if($result){
            return redirect()->route('home.index');
        }
        else{
            return redirect()->route('signin.index')->with('alert-success', 'Username or password is not valid');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
