<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeveloperRequest;
use App\MOdeveloper;
use Illuminate\Http\Request;

class COadmindeveloper extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $developer = MOdeveloper::all();
        return view('backend.developerlist', compact('developer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('backend.adddeveloper');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DeveloperRequest $request)
    {
        //
        $developer = new MOdeveloper();
        $developer->name = $request->name;
        $developer->description = $request->description;
        $developer->save();

        return redirect()->route('admindeveloper.index')->with('alert-success', 'Data Berhasil Disimpan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $developer = MOdeveloper::findOrFail($id);
        return view('backend.editdeveloper', compact('developer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $developer = MOdeveloper::findOrFail($id);
        $developer->name = $request->name;
        $developer->description = $request->description;
        $developer->save();
        return redirect()->route('admindeveloper.index')->with('alert-success', 'Data Berhasil Diubah.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $developer = MOdeveloper::findOrFail($id);
        $developer->delete();
        return redirect()->route('admindeveloper.index')->with('alert-success', 'Data Berhasil Dihapus.');
    }
}
