<?php

namespace App\Http\Controllers;

use App\MOage;
use Illuminate\Http\Request;

class COadminage extends Controller
{
    //

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $age = MOage::all();
        return view('backend.listage', compact('age'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('backend.addage');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $developer = new MOage();
        $developer->name = $request->name;
        $developer->description = $request->description;

        $file = $request ->file('file'); //request file gambar

        $fileName = $file->getClientOriginalName(); //ambil nama asli gambar
        $unique_name = time().$fileName; //make new name biar ngga bentrok kalau nama sama
        $request->file('file')->move("upload/rating/",$unique_name); // dari yang request di pindahin folder upload game

        $developer->picture = $unique_name; //game picture di db di isi dengan unique name

        $developer->save();

        return redirect()->route('adminage.index')->with('alert-success', 'Data Berhasil Disimpan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $age = MOage::findOrFail($id);
        return view('backend.editage', compact('age'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $developer = MOage::findOrFail($id);
        $developer->name = $request->name;
        $developer->description = $request->description;

        if($request->file('file') == "") //kalau gambar tidak ada
        {
            $developer->picture= $developer->picture; //tetap gambar & nama yang sama
        }
        else
        {
            $file = $request->file('file');
            $fileName = $file->getClientOriginalName();
            $unique_name = time().$fileName;
            $request->file('file')->move("upload/rating/",$unique_name);

            $developer->picture = $unique_name;
        }

        $developer->save();
        return redirect()->route('adminage.index')->with('alert-success', 'Data Berhasil Diubah.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $developer = MOage::findOrFail($id);
        $developer->delete();
        return redirect()->route('adminage.index')->with('alert-success', 'Data Berhasil Dihapus.');
    }
}
