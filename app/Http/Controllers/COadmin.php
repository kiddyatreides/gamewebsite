<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class COadmin extends Controller
{
    //

    public function index(){

        if (!session()->has('tokenadmin')) {
            return redirect()->route('adminsignin.index');
        }
        else {
            return view('backend.adminhome'); //menampilkan frontend home
        }

    }
}
