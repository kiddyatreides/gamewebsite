# Game Website Blog

This is blog game website that we can see the game detail and also rate the game.

## Getting Started

Make sure you using Laravel 5.4

### Prerequisites


```
- Laravel 5.4
- Composer
- PHP >= 5.6
- XAMPP Last Version
```

### Installing


```
1. open the cmd at the project folder and write "composer update"
2. Import the database SQL at Database Folder
```


## Deployment

```
php artisan serve
```

## Built With

* [Laravel](http://laravel.com) - The web framework used
* [Maven](https://w3layouts.com) - W3Layouts GameZone HTML


## Versioning

Version v1.2

## Authors

* **M Hudya Ramadhana** - [Kiddy Repo](https://github.com/kiddyatreides)

## License

This project is licensed personal by Me. If you want to use this as learning and study you are permitted.

