<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'COhome@index');
//Route::get('/about', 'COgame@about');
//Route::get('/gamelist','COgame@gamelist');
Route::get('/contact','COgame@contact');
Route::get('/game/{id}','COgame@show');

Route::resource('managegame','COmanagegame');
Route::resource('admingamelist','COadmingame');

//hudya
Route::resource('admindeveloper','COadmindeveloper');
Route::resource('admingenre','COadmingenre');
Route::resource('adminplatform','COadminplatform');
Route::resource('adminage','COadminage');

//baru
Route::resource('searchgame','COgamesearch');
//

//end hudya

Route::resource('game','COgame');
Route::resource('home','COhome');
Route::resource('signin','COsignin');
Route::resource('signup','COsignup'); //bikin controller dengan --resource
Route::resource('adminsignup','COadminsignup'); //view, controller
Route::resource('adminsignin','COadminsignin');
Route::resource('signout','COsignout');
Route::resource('adminsignout','COadminsignout');
Route::resource('adminlist','COadminlist');
