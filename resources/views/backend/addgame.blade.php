@extends('backend.base')
@section('content')

        <!--banner-->
<div class="banner">

    <h2>
        <a href="index.html">Home</a>
        <i class="fa fa-angle-right"></i>
        <span>Add Game</span>
    </h2>
</div>
<!--//banner-->

    <div class="grid-form">
        <div class="grid-form1">
            <h3 id="forms-example" class="">Form Game</h3>
            <form action="{{route('admingamelist.store')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="exampleInputEmail1">Game Title</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Email" name="game_title" required="">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Game Description</label>
                    <textarea type="text" class="form-control" id="exampleInputPassword1" placeholder="Password" name="description"></textarea>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Rating</label>
                    <select class="form-control" name="rating">
                        @foreach($age as $x2)
                            <option value="{{$x2->id}}">{{$x2->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Genre</label>
                    <select class="form-control" name="genre">
                        @foreach($genre as $x2)
                            <option value="{{$x2->id}}">{{$x2->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Platform</label>
                    <select class="form-control" name="platform">
                        @foreach($platform as $x2)
                            <option value="{{$x2->id}}">{{$x2->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Developer</label>
                    <select class="form-control" name="developer">
                        @foreach($developer as $x2)
                            <option value="{{$x2->id}}">{{$x2->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Price</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nama" name="price" required="">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Game Release</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nama" name="release" required="">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Game Access</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nama" name="access" required="">
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Game Picture</label>
                    <input type="file" class="form-control" id="exampleInputEmail1" placeholder="Nama" name="gambar" required="">
                </div>

                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
    </div>
@endsection