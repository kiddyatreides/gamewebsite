@extends('backend.base')
@section('content')

    <div class="content-top">
        <div class="col-md-12 ">
            <div class="content-top-1">
                <table class="table table-bordered" id="data">
                    <h3>Admin List</h3>

                    <br>
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Id Admin</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $no=1; ?>
                    @foreach($admin as $value)
                        <tr>
                            <th scope="row">{{ $no++ }}</th>
                            <td>{{ $value->id_admin }}</td>
                            <td>{{ $value->fName }}</td>
                            <td>{{ $value->lName }}</td>
                            <td>{{ $value->email }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

    </div>

@endsection