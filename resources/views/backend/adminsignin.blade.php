
<!DOCTYPE HTML>
<html>
<head>
    <title>Admin Sign In - KIDS.GO</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Minimal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <link href="/assets/backend/css/bootstrap.min.css" rel='stylesheet' type='text/css' />
    <!-- Custom Theme files -->
    <link href="/assets/backend/css/style.css" rel='stylesheet' type='text/css' />
    <link href="/assets/backend/css/font-awesome.css" rel="stylesheet">
    <script src="/assets/backend/js/jquery.min.js"> </script>
    <script src="/assets/backend/js/bootstrap.min.js"> </script>
</head>
<body>
<div class="login">
    <h1><a href="index.html">KIDS.GO </a></h1>
    <div class="login-bottom">
        <h2>Sign In</h2>

        <form action="{{route('adminsignin.store')}}" method="post"> <!-- isi csrf di bagian form-->

            {{csrf_field()}}

            <div class="col-md-6">
                <div class="login-mail">
                    <input type="text" placeholder="Email" required="" name="email">
                    <i class="fa fa-envelope"></i>
                </div>
                <div class="login-mail">
                    <input type="password" placeholder="Password" required="" name="password">
                    <i class="fa fa-lock"></i>
                </div>

                <!--<a class="news-letter " href="#">
                    <label class="checkbox1"><input type="checkbox" name="checkbox" ><i> </i>Forget Password</label>
                </a> -->


            </div>
            <div class="col-md-6 login-do">
                <label class="hvr-shutter-in-horizontal login-sub">
                    <input type="submit" value="login">
                </label>
                <center><p><b>Do not have an account? Contact your Supervisor</b></p></center>
               <!-- <a href="{{route('adminsignup.index')}}" class="hvr-shutter-in-horizontal">Signup</a> -->
            </div>

            <div class="clearfix"> </div>
        </form>
    </div>
</div>
<!---->
<div class="copy-right">
    <p> &copy; 2017 KIDS.GO </p>
</div>
<!---->
<!--scrolling js-->
<script src="/assets/backend/js/jquery.nicescroll.js"></script>
<script src="/assets/backend/js/scripts.js"></script>
<!--//scrolling js-->
</body>
</html>

