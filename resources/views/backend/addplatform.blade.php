@extends('backend.base')
@section('content')

        <!--banner-->
<div class="banner">

    <h2>
        <a href="index.html">Home</a>
        <i class="fa fa-angle-right"></i>
        <span>Add Game</span>
    </h2>
</div>
<!--//banner-->

<div class="grid-form">
    <div class="grid-form1">
        <h3 id="forms-example" class="">Form Game</h3>
        <form action="{{route('adminplatform.store')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label for="exampleInputEmail1">Name</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Name" name="name" required="">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Description</label>
                <textarea type="text" class="form-control" id="exampleInputPassword1" placeholder="" name="description" ></textarea>
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
</div>
@endsection