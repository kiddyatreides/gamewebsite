@extends('backend.base')
@section('content')

        <!--banner-->
<div class="banner">

    <h2>
        <a href="index.html">Home</a>
        <i class="fa fa-angle-right"></i>
        <span>Game List</span>
    </h2>
</div>
<!--//banner-->

    <div class="content-top">
        <div class="col-md-12 ">
            <div class="content-top-1">
                <table class="table table-bordered" id="data">
                    <h3>Daftar Game</h3>
                    @if(Session::has('alert-success'))
                        <div class="alert alert-success">
                            {{ Session::get('alert-success') }}
                        </div>
                    @endif
                    <br>
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Game Title</th>
                        <th>Game Description</th>
                        <th>Date</th>
                        <th>Genre</th>
                        <th>Developer</th>
                        <th>Action</th>
                        <!--<th>Admin</th>
                        <th>Developer</th>
                        <th>Price</th>
                        <th>Game Release</th>
                        <th>Game Access</th>
                        <th>Picture</th>-->
                    </tr>
                    </thead>
                    <tbody>
                    <?php $no=1; ?>
                    @foreach($game as $value)
                        <tr>
                            <th scope="row">{{ $no++ }}</th>
                            <td>{{ $value->game_title }}</td>
                            <td>{{ $value->game_description }}</td>
                            <td>{{ $value->created_at }}</td>
                            <td>{{ $value->nama_genre }}</td>
                            <td>{{ $value->nama_developer }}</td>
                            <td>
                                <form method="POST" action="{{ route('admingamelist.destroy', $value->id) }}" accept-charset="UTF-8">
                                    <input name="_method" type="hidden" value="DELETE">
                                    <input name="_token" type="hidden" value="{{ csrf_token() }}">
                                    <a class="btn-sm btn-info"  href="{{route('admingamelist.edit', $value->id)}}">Edit Game</a>
                                    <input type="submit" class="btn btn-warning btn-sm" onclick="return confirm('Anda yakin akan menghapus data ?');" value="Delete">
                                </form></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

    </div>
@endsection