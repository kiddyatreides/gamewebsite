@extends('backend.base')
@section('content')

        <!--banner-->
<div class="banner">

    <h2>
        <a href="index.html">Home</a>
        <i class="fa fa-angle-right"></i>
        <span>Edit Game</span>
    </h2>
</div>
<!--//banner-->

    <div class="grid-form">
        <div class="grid-form1">
            <h3 id="forms-example" class="">Form Game</h3>
            <form action="{{route('admingamelist.update', $game->id)}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="exampleInputEmail1">Game Title</label>
                    <input type="hidden" class="form-control" id="exampleInputEmail1" placeholder="Email" name="id" required="" value="{{$game->id}}">
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Email" name="game_title" required="" value="{{ $game->game_title }}">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Game Description</label>
                    <textarea type="text" class="form-control" id="exampleInputPassword1" placeholder="Password" name="description" required="">{{ $game->game_description }}</textarea>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Rating</label>
                    <select class="form-control" name="rating">
                        <option value="{{$game->id_rating}}">{{$game->rating}}</option>
                        @foreach($rating as $x)
                            <option value="{{$x->id}}">{{$x->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Genre</label>
                    <select class="form-control" name="genre">
                        <option value="{{$game->id_genre}}">{{$game->nama_genre}}</option>
                        @foreach($genre as $x2)
                            <option value="{{$x2->id}}">{{$x2->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Platform</label>
                    <select class="form-control" name="platform">
                        <option value="{{$game->id_platform}}">{{$game->platform}}</option>
                        @foreach($platform as $x2)
                            <option value="{{$x2->id}}">{{$x2->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Developer</label>
                    <select class="form-control" name="developer">
                        <option value="{{$game->id_developer}}">{{$game->developer}}</option>
                        @foreach($developer as $x2)
                            <option value="{{$x2->id}}">{{$x2->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Price</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nama" name="price" required="" value="{{$game->game_price}}">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Game Release</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nama" name="release" required="" value="{{$game->game_release}}">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Game Access</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nama" name="access" required="" value="{{$game->game_access}}">
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Game Picture</label>
                    <br>
                    <img src="{{asset('upload/game/'.$game->game_picture)}}" style="height: 150px; width: 150px;"/>
                    <input type="file" class="form-control" id="exampleInputEmail1" placeholder="Nama" name="gambar">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
                {{ method_field('PUT') }}
            </form>
        </div>
    </div>
@endsection