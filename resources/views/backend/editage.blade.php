@extends('backend.base')
@section('content')

        <!--banner-->
<div class="banner">

    <h2>
        <a href="index.html">Home</a>
        <i class="fa fa-angle-right"></i>
        <span>Add Game</span>
    </h2>
</div>
<!--//banner-->

<div class="grid-form">
    <div class="grid-form1">
        <h3 id="forms-example" class="">Form Game</h3>
        <form action="{{route('adminage.update', $age->id)}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label for="exampleInputEmail1">Name</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Name" name="name" required="" value="{{$age->name}}">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Description</label>
                <textarea type="text" class="form-control" id="exampleInputPassword1" placeholder="" name="description" required="">{{$age->description}}</textarea>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Description</label>
                <input type="file" class="form-control" id="exampleInputEmail1" placeholder="Name" name="file">
                <br>
                <img src="{{asset('upload/rating/'.$age->picture)}}" style="height: 150px; width: 150px;"/>
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
            {{ method_field('PUT') }}
        </form>
    </div>
</div>
@endsection