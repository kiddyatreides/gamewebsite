<!DOCTYPE HTML>
<html>
<head>
    <title>KIDS.GO Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Minimal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="/assets/backend/application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <link href="/assets/backend/css/bootstrap.min.css" rel='stylesheet' type='text/css' />
    <!-- Custom Theme files -->
    <link href="/assets/backend/css/style.css" rel='stylesheet' type='text/css' />
    <link href="/assets/backend/css/font-awesome.css" rel="stylesheet">
    <script src="/assets/backend/js/jquery.min.js"> </script>
    <!-- Mainly scripts -->
    <script src="/assets/backend/js/jquery.metisMenu.js"></script>
    <script src="/assets/backend/js/jquery.slimscroll.min.js"></script>

    <!-- Script JQuery Data Table -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script>

    <!--tiny MCE base -->
    <script src="/assets/backend/tinymce/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>

    <!-- Custom and plugin javascript -->
    <link href="/assets/backend/css/custom.css" rel="stylesheet">
    <script src="/assets/backend/js/custom.js"></script>
    <script src="/assets/backend/js/screenfull.js"></script>
    <script>
        $(function () {
            $('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

            if (!screenfull.enabled) {
                return false;
            }



            $('#toggle').click(function () {
                screenfull.toggle($('#container')[0]);
            });



        });
    </script>

    <!----->

    <!--pie-chart--->
    <script src="/assets/backend/js/pie-chart.js" type="text/javascript"></script>
    <script type="/assets/backend/text/javascript">

        $(document).ready(function () {
            $('#demo-pie-1').pieChart({
                barColor: '#3bb2d0',
                trackColor: '#eee',
                lineCap: 'round',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });

            $('#demo-pie-2').pieChart({
                barColor: '#fbb03b',
                trackColor: '#eee',
                lineCap: 'butt',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });

            $('#demo-pie-3').pieChart({
                barColor: '#ed6498',
                trackColor: '#eee',
                lineCap: 'square',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });


        });

    </script>
    <!--skycons-icons-->
    <script src="/assets/backend/js/skycons.js"></script>
    <!--//skycons-icons-->
</head>
<body>
<div id="wrapper">

    <!----->
    <nav class="navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <h1> <a class="navbar-brand" href="/admin">KIDS.GO Admin</a></h1>
        </div>
        <div class=" border-bottom">
            <div class="full-left">
                <section class="full-top">
                    <button id="toggle"><i class="fa fa-arrows-alt"></i></button>
                </section>

                <div class="clearfix"> </div>
            </div>


            <!-- Brand and toggle get grouped for better mobile display -->

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="drop-men" >
                <ul class=" nav_1">


                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle dropdown-at" data-toggle="dropdown"><span class=" name-caret">{{session()->get ('nameadmin')}}<i class="caret"></i></span><img src="/assets/backend/images/wo.jpg"></a>
                        <ul class="dropdown-menu " role="menu">
                            <li><a href="profile.html"><i class="fa fa-user"></i>Edit Profile</a></li>
                            <li><a href="{{route('adminsignout.index')}}"><i class="fa fa-user"></i>Sign out</a></li>
                        </ul>
                    </li>

                </ul>
            </div><!-- /.navbar-collapse -->
            <div class="clearfix">

            </div>

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">

                        <li>
                            <a href="/admin" class=" hvr-bounce-to-right"><i class="fa fa-dashboard nav_icon "></i><span class="nav-label">Dashboards</span> </a>
                        </li>

                        <li>
                            <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-indent nav_icon"></i> <span class="nav-label">Manage Game</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="{{route('admingamelist.index')}}" class=" hvr-bounce-to-right"><i class="fa fa-align-left nav_icon"></i>Game List</a></li>
                                <li><a href="{{route('admingamelist.create')}}" class="hvr-bounce-to-right"><i class="fa fa-file-text-o nav_icon"></i>Create Game Review</a></li>

                            </ul>
                        </li>

                        <li>
                            <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-list nav_icon"></i> <span class="nav-label">Manage user</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="maps.html" class=" hvr-bounce-to-right"><i class="fa fa-map-marker nav_icon"></i>User List</a></li>
                                <li><a href="{{route('adminlist.index')}}"class=" hvr-bounce-to-right"> <i class="fa fa-info-circle nav_icon"></i>Admin List</a></li>
                            </ul>
                        </li>

                        <li>
                            <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-cog nav_icon"></i> <span class="nav-label">Settings</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="{{route('adminsignout.index')}}" class=" hvr-bounce-to-right"><i class="fa fa-sign-in nav_icon"></i>Signout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
    </nav>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="content-main">



            @yield('content')
            <!--//content-->



            <!---->
            <div class="copy">
                <p> &copy; 2017 KIDS.GO </p>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>
<!---->

<!--data tables-->
<script type="text/javascript">
    $(document).ready(function(){
        $('#data').DataTable(); <!-- # baca ke id, kalau . ke class-->
    });
</script>

<!--scrolling js-->
<script src="/assets/backend/js/jquery.nicescroll.js"></script>
<script src="/assets/backend/js/scripts.js"></script>
<!--//scrolling js-->
<script src="/assets/backend/js/bootstrap.min.js"> </script>
</body>
</html>


