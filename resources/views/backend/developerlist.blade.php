@extends('backend.base')
@section('content')

    <div class="content-top">
        <div class="col-md-12 ">
            <div class="content-top-1">
                <table class="table table-bordered" id="data">
                    <h3>Admin List</h3>
                    @if(Session::has('alert-success'))
                        <div class="alert alert-success">
                            {{ Session::get('alert-success') }}
                        </div>
                    @endif
                    <br>
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Aksi</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $no=1; ?>
                    @foreach($developer as $value)
                        <tr>
                            <th scope="row">{{ $no++ }}</th>
                            <td>{{ $value->name }}</td>
                            <td>{{ $value->description }}</td>
                            <td>
                                <form method="POST" action="{{ route('admindeveloper.destroy', $value->id) }}" accept-charset="UTF-8">
                                    <input name="_method" type="hidden" value="DELETE">
                                    <input name="_token" type="hidden" value="{{ csrf_token() }}">
                                    <a class="btn-sm btn-info"  href="{{route('admindeveloper.edit', $value->id)}}">Edit Developer</a>
                                    <input type="submit" class="btn btn-warning btn-sm" onclick="return confirm('Anda yakin akan menghapus data ?');" value="Delete">
                                </form></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

    </div>

@endsection