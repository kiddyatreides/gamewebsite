@extends('frontend.base')
@section('content')


    <div class ="events-w3layouts">
        <h2>Game List</h2>
        <div class="clearfix">

        </div>

    </div>
    <div class="events-section2-agileinfo">

        <div class="container">
            @foreach($game as $value) <!-- Looping variable game di namakan sebagai value -->
            <div class="blog1-w3ls">
                <div class="col-md-4 blog-image-w3l">
                    <a href="single.html"><img src="{{ asset('upload/game/'.$value->game_picture) }}" alt=" " /></a>
                </div>
    <div class="col-md-8">
        <a href="single.html"><h2>{{ $value->game_title }}</h2></a>
        <div class="item_info">
            <ul>
                <li><a href="#"><i class="glyphicon glyphicon-user"></i>{{ $value->name_admin }}</a></li>
                <li><i class="glyphicon glyphicon-calendar"></i>{{ $value->created_at }}</li>
                <li><a href="#"><i class="glyphicon glyphicon-comment"></i>{{ $value->genre }}</a></li>
            </ul>
        </div>
        <p> <b> Game Platform : </b>{{ $value->platform }}</p>
        <p> <b> Game Developer : </b>{{ $value->developer }}</p>
        <p> <b> Game Rating : </b> <br>
            <?php
            $rating3 = DB::table('tb_user_rating')
                        ->join('tb_game', 'tb_user_rating.id_game', '=','tb_game.id')
                        ->select(DB::raw('COUNT( tb_user_rating.average ) as jumlah')
                            , DB::raw('AVG( tb_user_rating.average ) as avgg'))
                        ->where('tb_user_rating.id_game', '=' , $value->id)
                        ->get();
                        ?>
            @foreach($rating3 as $a)
                @if(($a->jumlah) == 0)
                    Not Yet Rating
                @endif
                @if(($a->jumlah) >= 1)
                    @if(($a->avgg) == 0)
                        @foreach($agelow as $x)
                            <img style="height: 100px; width: 100px;" src="{{ asset('upload/rating/'.$x->picture) }}" alt=" " />
                        @endforeach
                    @endif
                    @if((($a->avgg) <= 1.3333) && ($a->avgg) > 0)
                        @foreach($agemed as $x)
                            <img style="height: 100px; width: 100px;" src="{{ asset('upload/rating/'.$x->picture) }}" alt=" " />
                        @endforeach
                    @endif
                    @if((($a->avgg) < 3) && ($a->avgg) > 1.3333)
                        @foreach($agemed as $x)
                            <img style="height: 100px; width: 100px;" src="{{ asset('upload/rating/'.$x->picture) }}" alt=" " />
                        @endforeach
                    @endif
                @endif
                @endforeach
            <br>



        <a href="{{route('game.show',$value->id)}}" class="blog-read" >Read More</a>
    </div>
    <div class="clearfix"></div>
                <hr style="border-style: solid none; border-width: 1px 0; margin: 18px 0;">
    </div>

            @endforeach
            {{ $game->links() }}

   <!-- <div class="blog2-w3ls">
        <div class="col-md-4 blog-image-w3l">
            <a href="single.html"><img src="/assets/frontend/images/ng2.jpg" alt=" " /></a>
        </div>
        <div class="col-md-8 blog-text-w3ls">
            <a href="single.html"><h4>In finibus vel metus</h4></a>
            <div class="item_info">
                <ul>
                    <li><a href="#"><i class="glyphicon glyphicon-user"></i>Admin</a></li>
                    <li><i class="glyphicon glyphicon-calendar"></i>17.Aug.2017</li>
                    <li><a href="#"><i class="glyphicon glyphicon-comment"></i>20 Comments</a></li>
                    <li><a href="#"><i class="glyphicon glyphicon-heart"></i>300 Likes</a></li>
                </ul>
            </div>
            <p>Aliquam suscipit neque massa, eu maximus felis gravida vel. Vestibulum lacinia risus risus, ut iaculis felis fermentum id. Cras at vulputate velit, vitae vestibulum augue. Etiam lorem nunc, mattis ac dignissim sit amet, varius et ex. Phasellus eleifend nibh justo, pulvinar cursus sapien commodo non.</p>
            <a href="single.html" class="blog-read" >Read More</a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="blog3-w3ls">
        <div class="col-md-4 blog-image-w3l">
            <a href="single.html"><img src="/assets/frontend/images/ng3.jpg" alt=" " /></a>
        </div>
        <div class="col-md-8 blog-text-w3ls">
            <a href="single.html"><h4>Nam eget ligula eu</h4></a>
            <div class="item_info">
                <ul>
                    <li><a href="#"><i class="glyphicon glyphicon-user"></i>Admin</a></li>
                    <li><i class="glyphicon glyphicon-calendar"></i>24.Aug.2015</li>
                    <li><a href="#"><i class="glyphicon glyphicon-comment"></i>20 Comments</a></li>
                    <li><a href="#"><i class="glyphicon glyphicon-heart"></i>300 Likes</a></li>
                </ul>
            </div>
            <p>Aliquam suscipit neque massa, eu maximus felis gravida vel. Vestibulum lacinia risus risus, ut iaculis felis fermentum id. Cras at vulputate velit, vitae vestibulum augue. Etiam lorem nunc, mattis ac dignissim sit amet, varius et ex. Phasellus eleifend nibh justo, pulvinar cursus sapien commodo non.</p>
            <a href="single.html" class="blog-read" >Read More</a>
        </div>
        <div class="clearfix"></div>
    </div> -->

    </div>
    </div>


@endsection
