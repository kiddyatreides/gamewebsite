@extends('frontend.base') <!--memanggil dari base -->
@section('content')


<!-- new games -->
<div class="new-w3-agile">
    <div class="container">
        <h3>New Games</h3>
        <div class="col-md-3 new-grid-w3l view view-eighth">
            <img src="/assets/frontend/images/ng1.jpg" alt=" " />
            <div class="mask">
                <a href="single.html"><h4>Click here</h4></a>
                <p>To learn more about this</p>
            </div>
        </div>
        <div class="col-md-3 new-grid-w3l view view-eighth">
            <img src="/assets/frontend/images/ng2.jpg" alt=" " />
            <div class="mask">
                <a href="single.html"><h4>Click here</h4></a>
                <p>To learn more about this</p>
            </div>
        </div>
        <div class="col-md-3 new-grid-w3l view view-eighth">
            <img src="/assets/frontend/images/ng3.jpg" alt=" " />
            <div class="mask">
                <a href="single.html"><h4>Click here</h4></a>
                <p>To learn more about this</p>
            </div>
        </div>
        <div class="col-md-3 new-grid-w3l view view-eighth">
            <img src="/assets/frontend/images/ng4.jpg" alt=" " />
            <div class="mask">
                <a href="single.html"><h4>Click here</h4></a>
                <p>To learn more about this</p>
            </div>
        </div>
        <div class="col-md-3 new-grid-agile view view-eighth">
            <img src="/assets/frontend/images/ng5.jpg" alt=" " />
            <div class="mask">
                <a href="single.html"><h4>Click here</h4></a>
                <p>To learn more about this</p>
            </div>
        </div>
        <div class="col-md-3 new-grid-agile view view-eighth">
            <img src="/assets/frontend/images/ng6.jpg" alt=" " />
            <div class="mask">
                <a href="single.html"><h4>Click here</h4></a>
                <p>To learn more about this</p>
            </div>
        </div>
        <div class="col-md-3 new-grid-agile view view-eighth">
            <img src="/assets/frontend/images/ng7.jpg" alt=" " />
            <div class="mask">
                <a href="single.html"><h4>Click here</h4></a>
                <p>To learn more about this</p>
            </div>
        </div>
        <div class="col-md-3 new-grid-agile view view-eighth">
            <img src="/assets/frontend/images/ng8.jpg" alt=" " />
            <div class="mask">
                <a href="single.html"><h4>Click here</h4></a>
                <p>To learn more about this</p>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- //new games-->


@endsection