<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>User Signup</title>
    <!-- custom-theme -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Esteem Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //custom-theme -->
    <link href="/assets/frontenduser/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/assets/frontenduser/css/snow.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/assets/frontenduser/css/component.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/assets/frontenduser/css/style_grid.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/assets/frontenduser/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <!-- font-awesome-icons -->
    <link href="/assets/frontenduser/css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome-icons -->
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet">
</head>
<body>
<!-- /pages_agile_info_w3l -->

<div class="pages_agile_info_w3l">
    <!-- /login -->
    <div class="over_lay_agile_pages_w3ls two">
        <div class="registration">

            <div class="signin-form profile">
                <h2>Sign up Form</h2>

                @if(Session::has('alert-success'))
                    <div class="alert alert-sucess">
                        {{Session::get('alert-success')}}

                     </div>
                @endif

                <div class="login-form">
                    <form action="{{route('signup.store')}}" method="post">
                        {{csrf_field()}} <!-- input data harus memasuki csrf -->

                        <input type="text" name="fName" placeholder="First Name" required="">
                        <input type="text" name="lName" placeholder="Last Name" required="">
                        <input type="email" name="email" placeholder="E-mail" required="">
                        <input type="password" name="password" placeholder="Password" required="">
                        <select name="gender" id="selector1" class="form-control1">
                            <option>-- Gender --</option>
                            <option value="Male">Pria</option>
                            <option value="Female">Wanita</option>
                        </select>
                        <div class="tp">
                            <input type="submit" value="SIGN Up">
                        </div>
                    </form>
                </div>

                <p> By clicking Sign Up, I agree to your terms</p>

                <h6><a href="/">Back To Home</a><h6>
            </div>
        </div>
        <!--copy rights start here-->
        <div class="copyrights_agile two">
            <p>© 2017 KIDS.GO </p>
        </div>
        <!--copy rights end here-->
    </div>
</div>
<!-- /login -->
<!-- //pages_agile_info_w3l -->


<!-- js -->

<script type="text/javascript" src="/assets/frontenduser/js/jquery-2.1.4.min.js"></script>
<script src="/assets/frontenduser/js/modernizr.custom.js"></script>

<script src="/assets/frontenduser/js/classie.js"></script>
<script src="/assets/frontenduser/js/gnmenu.js"></script>
<script>
    new gnMenu( document.getElementById( 'gn-menu' ) );
</script>

<!-- //js -->

<script src="js/screenfull.js"></script>
<script>
    $(function () {
        $('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

        if (!screenfull.enabled) {
            return false;
        }



        $('#toggle').click(function () {
            screenfull.toggle($('#container')[0]);
        });
    });
</script>
<script src="/assets/frontenduser/js/jquery.nicescroll.js"></script>
<script src="/assets/frontenduser/js/scripts.js"></script>
<script src="/assets/frontenduser/js/snow.js"></script>
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
<script type="text/javascript" src="/assets/frontenduser/js/bootstrap-3.1.1.min.js"></script>


</body>
</html>