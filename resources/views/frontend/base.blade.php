
<!DOCTYPE html>
<html lang="en">
<head>
    <title>KIDS.GO - Kids Game Info</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Games Zone Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- css -->
    <link href="/assets/frontend/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link rel="/assets/frontend/stylesheet" href="css/flexslider.css" type="text/css" media="screen" property="" />
    <link href="/assets/frontend/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <!--// css -->
    <!-- font -->
    <link href='//fonts.googleapis.com/css?family=Josefin+Sans:400,100,100italic,300,300italic,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <!-- //font -->
    <script src="/assets/frontend/js/jquery.min.js"></script>
    <script src="/assets/frontend/js/bootstrap.js"></script>

    <!-- Script JQuery Data Table -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script>

</head>
<body>
<!-- Header -->
<div class="header">
    <!-- banner -->
    <div class="sub-banner">
        <!-- Navbar -->
        <nav class="navbar navbar-default">
            <div class="container">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a  href="/"><h1>KIDS.GO <img src="/assets/frontend/images/c1.png" alt=" " /></h1></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="hover-effect"><a href="/">Home</a></li>
                        <li class="hover-effect"><a href="/about">About</a></li>
                        <li class="hover-effect"><a href="/gamelist">Games</a></li>
                        <li class="hover-effect"><a href="/contact">Contact</a></li>


                 <!-- loop for admin page-->
                        @if(!session()->has('token'))

                        <li class="menu-login" role="presentation">
                            <a href="/signin" class="mma btn-sm btn-info" id="mma"><span class="glyphicon glyphicon-log-in"></span>
                                Sign In				</a>
                        </li>
                        <li class="menu-daftar" role="presentation">
                            <a href="{{route('signup.index')}}" class="mda btn-sm btn-success" id="mda"><span class="glyphicon glyphicon-user"></span>
                                Sign Up				</a>
                        </li>

                            @endif
                        @if(session()->has('token'))

                            <li class="menu-daftar" role="presentation">
                                <a href="#" class="mda btn-sm btn-success" id="mda"><span class="glyphicon glyphicon-user"></span>
                                    Hello, {{session()->get('name')}}</a>
                            </li>

                            <li class="menu-login" role="presentation">
                                <a href="{{route('signout.index')}}" class="mma btn-sm btn-info" id="mma"><span class="glyphicon glyphicon-log-in"></span>
                                    Sign Out</a>
                            </li>


                        @endif

                    </ul>
                </div>

            </div>
        </nav>
        <!-- Navbar End -->
    </div>
    <!-- banner -->

    <!-- Slider -->
    <div class="slider">
        <ul class="rslides" id="slider">
            <li>
                <img src="/assets/frontend/images/banner6.png" alt="" />
            </li>
            <li>
                <img src="/assets/frontend/images/banner5.jpg" alt="" />
            </li>
        </ul>
    </div>
    <!-- //Slider -->

</div>
<!-- Banner-Slider-JavaScript -->
<script src="/assets/frontend/js/responsiveslides.min.js"></script>
<script>
    $(function () {
        $("#slider").responsiveSlides({
            auto: true,
            nav: true,
            speed: 800,
            namespace: "callbacks",
            pager: true,
        });
    });
</script>
<!-- //Banner-Slider-JavaScript -->
<!-- //Header -->

<!-- Game searching-->
<div class="services-agileits-w3layouts" style="padding: 0;">
    <div style="background-color: #e8e8e8; padding: 3% 0;">
    <div class="container">
        <h3>Search Game</h3>
        <div class="container-fluid" >
            <div class="container wrapper" id="home-search">
                <div class="row">

                    <form method="post" action="{{route('searchgame.store')}}">
                        {{csrf_field()}}
                        <div class="form-inline center">
                            <div class="form-group desk col-md-2 col-sm-8 col-xs-8">
                                <input type="text" name="name" id="name" class="form-control" placeholder="Game Title" style="width:100%">
                            </div>
                            <div class="form-group col-md-2 col-sm-3 col-xs-12">
                                <select id="selectAge" class="form-control selectBox" name="age" style="width: 100%">
                                        <option value="">-- Age --</option>
                                        @foreach($age as $x)
                                        <option value="{{$x->id}}">{{$x->name}}</option>
                                        @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-2 col-sm-3 col-xs-12">
                                <select id="selectGenre" class="form-control selectBox" name="genre">
                                    <option value="">-- Genre --</option>
                                    @foreach($genre as $x)
                                        <option value="{{$x->id}}">{{$x->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-2 col-sm-3 col-xs-12 ">
                                <select id="selectPlatform"  class="form-control selectBox btnpl" name="platform" style="width: 100%">
                                    <option value="">-- Platform --</option>
                                    @foreach($platform as $x)
                                        <option value="{{$x->id}}">{{$x->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="row">
                                    <div class="form-group col-md-2 col-sm-3 col-xs-12 center-block">
                                        <button type="submit" class="btn btn-default btncustom col-md-9">Search</button>
                                    </div>
                                </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <hr>

    </div>
    </div>
</div>
<!-- //services-->






<!-- //Contains -->
<br>
<div class="container-fluid" >
    <div class="container wrapper" id="home-search">
        <div class="row">

            <form method="post" action="{{route('searchgame.store')}}">
                {{csrf_field()}}
                <div class="form-inline center">
                    <div class="form-group desk col-md-2 col-sm-8 col-xs-8">
                        <input type="text" name="name" id="name" class="form-control" placeholder="Game Title" style="width:100%">
                    </div>
                    <div class="form-group col-md-2 col-sm-3 col-xs-12">
                        <select id="selectAge" class="form-control selectBox" name="age" style="width: 100%">
                            <option value="">-- Age --</option>
                            @foreach($age as $x)
                                <option value="{{$x->id}}">{{$x->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-2 col-sm-3 col-xs-12">
                        <select id="selectGenre" class="form-control selectBox" name="genre">
                            <option value="">-- Genre --</option>
                            @foreach($genre as $x)
                                <option value="{{$x->id}}">{{$x->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-2 col-sm-3 col-xs-12 ">
                        <select id="selectPlatform"  class="form-control selectBox btnpl" name="platform" style="width: 70%">
                            <option value="">-- Platform --</option>
                            {{--@foreach($platform as $x)--}}
                            {{--<option value="{{$x->id}}">{{$x->name}}</option>--}}
                            {{--@endforeach--}}
                            <option value="">-jdaskhhhhhhhhhhhhhhhhh- Platform --</option>
                        </select>
                    </div>

                    <div class="row">
                    <div class="form-group col-md-2 col-sm-3 col-xs-12 center-block">
                    <button type="submit" class="btn btn-default btncustom col-md-9">Search</button>
                    </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@yield('content') <!--menyiapkan content untuk di masukan


<!-- //end contains -->


<!-- footer -->
<div class="footer">
    <div class="container">
        <div class="col-md-3 footer-left-w3">
            <h4>Contact</h4>
            <ul>
                <li><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span></li>
                <li><a href="mailto:example@mail.com"><h6>ex@mail.com</h6></a></li>
            </ul>
            <ul>
                <li><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span></li>
                <li><h6>+18045678834</h6></li>
            </ul>
            <ul>
                <li><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span></li>
                <li><h6>4th Avenue,London</h6></li>
            </ul>
            <ul>
                <li><span class="glyphicon glyphicon-phone-alt" aria-hidden="true"></span></li>
                <li><h6>(0033)6544 5453 644</h6></li>
            </ul>
        </div>
        <div class="col-md-5 footer-middle-w3">
            <h4>Latest Games</h4>
            <div class="col-md-3 img-w3-agile">
                <a href="single.html"><img src="/assets/frontend/images/ng1.jpg" alt=" " /></a>
            </div>
            <div class="col-md-3 img-w3-agile">
                <a href="single.html"><img src="/assets/frontend/images/ng2.jpg" alt=" " /></a>
            </div>
            <div class="col-md-3 img-w3-agile">
                <a href="single.html"><img src="/assets/frontend/images/ng3.jpg" alt=" " /></a>
            </div>
            <div class="col-md-3 img-w3-agile">
                <a href="single.html"><img src="/assets/frontend/images/ng4.jpg" alt=" " /></a>
            </div>
            <div class="col-md-3 img-w3-agile footer-middle-wthree">
                <a href="single.html"><img src="/assets/frontend/images/ng5.jpg" alt=" " /></a>
            </div>
            <div class="col-md-3 img-w3-agile footer-middle-wthree">
                <a href="single.html"><img src="/assets/frontend/images/ng6.jpg" alt=" " /></a>
            </div>
            <div class="col-md-3 img-w3-agile footer-middle-wthree">
                <a href="single.html"><img src="/assets/frontend/images/ng7.jpg" alt=" " /></a>
            </div>
            <div class="col-md-3 img-w3-agile footer-middle-wthree">
                <a href="single.html"><img src="/assets/frontend/images/ng8.jpg" alt=" " /></a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-md-4 footer-right-w3">
            <a href="/about"><h4>KIDS.GO <img src="/assets/frontend/images/f1.png" alt=" " /> </h4></a>
            <p>Donec lobortis diam eu auctor porta. Phasellus in elementum tortor, sit amet imperdiet urna pellentesque non risus porta.</p>
            <p class="agileinfo">Suspendisse convallis malesuada libero, non rutrum arcu pellentesque lacinia.</p>
        </div>
        <div class="clearfix"></div>
        <div class="copyright">
            <p>&copy; 2017 KIDS.GO </p>
        </div>
    </div>
</div>

<!--data tables-->
<script type="text/javascript">
    $(document).ready(function(){
        $('#data').DataTable(); <!-- # baca ke id, kalau . ke class-->
    });
</script>
<!-- //footer -->
</body>
</html>