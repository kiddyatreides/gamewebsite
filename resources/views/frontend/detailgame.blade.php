@extends('frontend.base')
@section('content')
        <!-- single -->
<div class="blog">

    <div class="container">
        <div class="col-md-12">
            @if(Session::has('alert-success'))
                <div class="alert alert-success">
                    <center>{{ Session::get('alert-success') }}</center>
                </div>
            @endif
            @foreach($game as $x)
            <div class="">
                <div class="">
                    <center>   <img src="{{asset('upload/game/'.$x->game_picture)}}" alt=" " class="img-responsive" style="height: 250px; width: 250px;"/> </center>
                </div>

                <br>
                <h1></h1>
                {{--<ul>--}}
                {{--<li><span class="glyphicon glyphicon-user" aria-hidden="true"></span><a href="#">{{$game->nama_admin}}</a><i>|</i></li>--}}
                {{--<li><span class="glyphicon glyphicon-heart" aria-hidden="true"></span><a href="#">20</a><i>|</i></li>--}}
                {{--<li><span class="glyphicon glyphicon-tag" aria-hidden="true"></span><a href="#">5</a><i>|</i></li>--}}
                {{--<li><span class="glyphicon glyphicon-edit" aria-hidden="true"></span>Voluptatibus</li>--}}
                {{--</ul>--}}
                <br>


                <div class="box-header with-border bgblue2">
                    <h3 class="box-title">Game Information</h3>
                </div>

                <table class="table table-striped table-bordered table-advance table-hover">
                    <thead>
                    <tr>
                        <th>
                            <i class="fa fa-briefcase"></i> Game </th>
                        <th class="hidden-xs">
                            <i class="fa fa-question"></i> Description </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            Title
                        </td>
                        <td class="hidden-xs">
                            {{$x->game_title}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Platform
                        </td>
                        <td class="hidden-xs">
                                {{$x->platform}}
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Genre
                        </td>
                        <td class="hidden-xs"> {{$x->genre}}

                        </td>
                    </tr>

                    <tr>
                        <td>
                            Developer
                        </td>
                        <td class="hidden-xs">
                            {{$x->developer}}
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Price
                        </td>
                        <td class="hidden-xs"> {{$x->game_price}}

                        </td>
                    </tr>

                    <tr>
                        <td>
                            Release Date
                        </td>
                        <td class="hidden-xs"> {{$x->game_release}}

                        </td>
                    </tr>

                    <tr>
                        <td>
                            Game Description
                        </td>
                        <td>{!! $x->game_description !!}

                        </td>
                    </tr>

                    <tr>
                        <td>
                            Game Access
                        </td>
                        <td class="hidden-xs"> <a href="{{$x->game_access}}" target="_blank">Click Here</a>

                        </td>
                    </tr>


                    </tbody>
                </table>
            </div>
            @endforeach

            <br>
            <br>
                <div class="box-header with-border bgblue2">
                    <h3 class="box-title">Current Rating</h3>
                </div>

        </div>
        <div class="agileits_share">

            <table class="table table-striped table-bordered table-advance table-hover">
                <thead>
                <tr>
                    <th>
                        <i class="fa fa-briefcase"></i> Value </th>
                    <th class="hidden-xs">
                        <i class="fa fa-question"></i> Score </th>
                </tr>
                </thead>
                <tbody>
                @foreach($rating as $a)
                <tr>
                    <td>
                        Violence
                    </td>
                    <td>
                        @if(($a->jumlah) == 0)
                            Not Yet Rating
                        @endif
                        @if(($a->jumlah) >= 1)
                                @if(($a->viol) == 0)
                                    Neutral
                                @endif
                                @if((($a->viol) <= 1.3333) && ($a->viol) > 0)
                                    Mid
                                @endif
                                @if((($a->viol) < 3) && ($a->viol) > 1.3333)
                                    Low
                                @endif
                        @endif


                    </td>
                </tr>
                <tr>
                    <td>
                        Language
                    </td>
                    <td >
                        @if(($a->jumlah) == 0)
                            Not Yet Rating
                        @endif
                        @if(($a->jumlah) >= 1)
                            @if(($a->lang) == 0)
                                Neutral
                            @endif
                            @if((($a->lang) <= 1.3333) && ($a->lang) > 0)
                                Mid
                            @endif
                            @if((($a->lang) < 3) && ($a->lang) > 1.3333)
                                Low
                            @endif
                        @endif

                    </td>
                </tr>

                <tr>
                    <td>
                        Fear & Horror
                    </td>
                    <td >
                        @if(($a->jumlah) == 0)
                            Not Yet Rating
                        @endif
                        @if(($a->jumlah) >= 1)
                            @if(($a->fh) == 0)
                                Neutral
                            @endif
                            @if((($a->fh) <= 1.3333) && ($a->fh) > 0)
                                Mid
                            @endif
                            @if((($a->fh) < 3) && ($a->fh) > 1.3333)
                                Low
                            @endif
                        @endif
                    </td>
                </tr>

                <tr>
                    <td>
                        Online Interaction
                    </td>
                    <td>
                        @if(($a->jumlah) == 0)
                            Not Yet Rating
                        @endif
                        @if(($a->jumlah) >= 1)
                            @if(($a->oi) == 0)
                                Neutral
                            @endif
                            @if((($a->oi) <= 1.3333) && ($a->oi) > 0)
                                Mid
                            @endif
                            @if((($a->oi) < 3) && ($a->oi) > 1.3333)
                                Low
                            @endif
                        @endif
                    </td>
                </tr>
                    <tr>
                        <td>
                            Rating
                        </td>
                        <td>
                            @if(($a->jumlah) == 0)
                                Not Yet Rating
                            @endif
                            @if(($a->jumlah) >= 1)
                                @if(($a->avgg) == 0)
                                    @foreach($agelow as $x)
                                        <img style="height: 100px; width: 100px;" src="{{ asset('upload/rating/'.$x->picture) }}" alt=" " />
                                    @endforeach
                                @endif
                                @if((($a->avgg) <= 1.3333) && ($a->avgg) > 0)
                                        @foreach($agemed as $x)
                                            <img style="height: 100px; width: 100px;" src="{{ asset('upload/rating/'.$x->picture) }}" alt=" " />
                                        @endforeach
                                @endif
                                @if((($a->avgg) < 3) && ($a->avgg) > 1.3333)
                                        @foreach($agemed as $x)
                                            <img style="height: 100px; width: 100px;" src="{{ asset('upload/rating/'.$x->picture) }}" alt=" " />
                                        @endforeach
                                @endif
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        @foreach($user as $us)
            @if(($us->user) < 1)
        @foreach($game as $x)
        <div class="agileits_reply">
            <h3>Rate The Game</h3>
            <i>  <p>Check each content description <a href="#" data-toggle="modal" data-target="#myModal">here</a></p> </i><br>
            <form action="{{route('game.store')}}" method="post">
                {{csrf_field()}}
                <input type="hidden" value="{{$x->id}}" name="id">
                <fieldset id="group1">
                    <label>Violence</label>&nbsp;&nbsp;&nbsp;<input type="radio" name="violence" value="0" /> Low
                    &nbsp;&nbsp;&nbsp;<input type="radio" name="violence" value="1" /> Medium&nbsp;&nbsp;&nbsp;<input type="radio" name="violence" value="2" /> High
                </fieldset>
                <br>
                <fieldset id="group2">
                    <label>Language</label>&nbsp;&nbsp;&nbsp;<input type="radio" name="lang" value="0" /> Low
                    &nbsp;&nbsp;&nbsp;<input type="radio" name="lang" value="1" /> Medium&nbsp;&nbsp;&nbsp;<input type="radio" name="lang" value="2" /> High
                </fieldset>
                <br>
                <fieldset id="group3">
                    <label>Fear & Horror</label>&nbsp;&nbsp;&nbsp;<input type="radio" name="fh" value="0" /> Low
                    &nbsp;&nbsp;&nbsp;<input type="radio" name="fh" value="1" /> Medium&nbsp;&nbsp;&nbsp;<input type="radio" name="fh" value="2" /> High
                </fieldset>
                <br>
                <fieldset id="group4">
                    <label>Online Interaction</label>&nbsp;&nbsp;&nbsp;<input type="radio" name="oi" value="0" /> Low
                    &nbsp;&nbsp;&nbsp;<input type="radio" name="oi" value="1" /> Medium&nbsp;&nbsp;&nbsp;<input type="radio" name="oi" value="2" /> High
                </fieldset>

                <h3>Leave a Review</h3>
                <textarea name="review" placeholder="Type your comment..." required=""></textarea>
                <input type="submit" value="Submit">
            </form>
        </div>
            @endforeach
            @endif
            @endforeach
    <br>
        <div class="jumbotron">
            <table class="table" id="data">
                <h3>Review <Game></Game></h3>
                <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Review</th>
                </tr>
                </thead>
                <?php $no = 1; ?>
                <tbody>
                @foreach($review as $x)
                <tr>
                    <td>{{$no++}}</td>
                    <td>{{$x->fName}}</td>
                    <td>{{$x->review}}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>

    <div class="col-md-5 wthree_blog_right">
        <div class="clearfix"> </div>
    </div>

    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog" style=" width: 90%; /* desired relative width */
  left: 5%; /* (100%-width)/2 */
  /* place center */
  margin-left:auto;
  margin-right:auto; ">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Rating Description</h4>
                </div>
                <div class="modal-body">
                    <table class="table">
                        <thead>
                        <tr>

                            <th><i class="fa fa-briefcase"></i><center>Content Descriptor </center> </th>
                            <th><i class="fa fa-question"></i><center>  Neutral</center>  </th>
                            <th><i class="fa fa-briefcase"></i><center>  Mild </center> </th>
                            <th><i class="fa fa-question"></i> <center> Low</center>  </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Violence</td>
                            <td> <ul class="list-group">
                                    <li class="list-group-item list-group-item-info">No depiction of blood</li>
                                    <li class="list-group-item list-group-item-info">No scenes containing physical harm (beating, stabbing, shooting, and etc)</li>
                                </ul>
                            </td>
                            <td> <ul class="list-group">
                                    <li class="list-group-item list-group-item-warning">Depic little blood with different color from the realistic blood (red)</li>
                                    <li class="list-group-item list-group-item-warning">No scenes containing physical harm (beating, stabbing, shooting, and etc)</li>
                                </ul>
                            </td>
                            <td> <ul class="list-group">
                                    <li class="list-group-item list-group-item-danger">May contain blood</li>
                                    <li class="list-group-item list-group-item-danger">May contain physical harm but no continuously</li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>Language</td>
                            <td> <ul class="list-group">
                                    <li class="list-group-item list-group-item-info">No rude language (vulgar, curse, and slur, and etc)</li>
                                    <li class="list-group-item list-group-item-info">No ethnicity discriminatory language (ethnicity, race, and religion)</li>
                                    <li class="list-group-item list-group-item-info">No Profanity language</li>
                                </ul>
                            </td>
                            <td> <ul class="list-group">
                                    <li class="list-group-item list-group-item-warning">No rude language (vulgar, curse, and slur, and etc)</li>
                                    <li class="list-group-item list-group-item-warning">No ethnicity discriminatory language (ethnicity, race, and religion)</li>
                                    <li class="list-group-item list-group-item-warning">No Profanity language</li>
                                </ul>
                            </td>
                            <td> <ul class="list-group">
                                    <li class="list-group-item list-group-item-danger">May contain some rude statement but un-continuously</li>
                                    <li class="list-group-item list-group-item-danger">No ethnicity discriminatory language (ethnicity, race, and religion)</li>
                                    <li class="list-group-item list-group-item-danger">No Profanity language</li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>Fear & Horror</td>
                            <td> <ul class="list-group">
                                    <li class="list-group-item list-group-item-info">No Fear & Horror</li>
                                </ul>
                            </td>
                            <td> <ul class="list-group">
                                    <li class="list-group-item list-group-item-warning">May contain insignificant Fear & Horror</li>
                                </ul>
                            </td>
                            <td> <ul class="list-group">
                                    <li class="list-group-item list-group-item-danger">Can contain Fear & Horror</li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>Online Interaction</td>
                            <td> <ul class="list-group">
                                    <li class="list-group-item list-group-item-info">No facility on Online Interaction</li>
                                </ul>
                            </td>
                            <td> <ul class="list-group">
                                    <li class="list-group-item list-group-item-warning">May involve multi-player</li>
                                </ul>
                            </td>
                            <td> <ul class="list-group">
                                    <li class="list-group-item list-group-item-danger">Can contain Online Interaction</li>
                                </ul>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- //single -->
@endsection