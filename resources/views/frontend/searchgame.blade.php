@extends('frontend.base')
@section('content')


    <div class ="events-w3layouts">
        <h2>Game List</h2>
        <div class="clearfix">

        </div>

    </div>
    <div class="events-section2-agileinfo">
        @if(Session::has('alert-success'))
            <div class="alert alert-success">
                <center>{{ Session::get('alert-success') }}</center>
            </div>
        @endif
        <div class="container">
            @foreach($games as $value) <!-- Looping variable game di namakan sebagai value -->
            <div class="blog1-w3ls">
                <div class="col-md-4 blog-image-w3l">
                    <a href="single.html"><img src="{{ asset('upload/game/'.$value->game_picture) }}" alt=" " /></a>
                </div>
                <div class="col-md-8">
                    <a href="single.html"><h2>{{ $value->game_title }}</h2></a>
                    <div class="item_info">
                        <ul>
                            <li><a href="#"><i class="glyphicon glyphicon-user"></i>{{ $value->name_admin }}</a></li>
                            <li><i class="glyphicon glyphicon-calendar"></i>{{ $value->created_at }}</li>
                            <li><a href="#"><i class="glyphicon glyphicon-comment"></i>{{ $value->genre }}</a></li>
                        </ul>
                    </div>
                    <p> <b> Game Description : </b>{{ $value->game_description }}</p>
                    <p> <b> Game Platform : </b>{{ $value->platform }}</p>
                    <br>
                    <p> <b> Game Rating : </b> <br><img style="height: 100px; width: 100px;" src="{{ asset('upload/rating/'.$value->rating) }}" alt=" " /></p> <!-- memakai src karena gambar nya dynamis (memakai banyak gambar)-->

                    <a href="{{route('game.show',$value->id)}}" class="blog-read" >Read More</a>
                </div>
                <div class="clearfix"></div>
            </div>

            @endforeach

                    <!-- <div class="blog2-w3ls">
        <div class="col-md-4 blog-image-w3l">
            <a href="single.html"><img src="/assets/frontend/images/ng2.jpg" alt=" " /></a>
        </div>
        <div class="col-md-8 blog-text-w3ls">
            <a href="single.html"><h4>In finibus vel metus</h4></a>
            <div class="item_info">
                <ul>
                    <li><a href="#"><i class="glyphicon glyphicon-user"></i>Admin</a></li>
                    <li><i class="glyphicon glyphicon-calendar"></i>17.Aug.2017</li>
                    <li><a href="#"><i class="glyphicon glyphicon-comment"></i>20 Comments</a></li>
                    <li><a href="#"><i class="glyphicon glyphicon-heart"></i>300 Likes</a></li>
                </ul>
            </div>
            <p>Aliquam suscipit neque massa, eu maximus felis gravida vel. Vestibulum lacinia risus risus, ut iaculis felis fermentum id. Cras at vulputate velit, vitae vestibulum augue. Etiam lorem nunc, mattis ac dignissim sit amet, varius et ex. Phasellus eleifend nibh justo, pulvinar cursus sapien commodo non.</p>
            <a href="single.html" class="blog-read" >Read More</a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="blog3-w3ls">
        <div class="col-md-4 blog-image-w3l">
            <a href="single.html"><img src="/assets/frontend/images/ng3.jpg" alt=" " /></a>
        </div>
        <div class="col-md-8 blog-text-w3ls">
            <a href="single.html"><h4>Nam eget ligula eu</h4></a>
            <div class="item_info">
                <ul>
                    <li><a href="#"><i class="glyphicon glyphicon-user"></i>Admin</a></li>
                    <li><i class="glyphicon glyphicon-calendar"></i>24.Aug.2015</li>
                    <li><a href="#"><i class="glyphicon glyphicon-comment"></i>20 Comments</a></li>
                    <li><a href="#"><i class="glyphicon glyphicon-heart"></i>300 Likes</a></li>
                </ul>
            </div>
            <p>Aliquam suscipit neque massa, eu maximus felis gravida vel. Vestibulum lacinia risus risus, ut iaculis felis fermentum id. Cras at vulputate velit, vitae vestibulum augue. Etiam lorem nunc, mattis ac dignissim sit amet, varius et ex. Phasellus eleifend nibh justo, pulvinar cursus sapien commodo non.</p>
            <a href="single.html" class="blog-read" >Read More</a>
        </div>
        <div class="clearfix"></div>
    </div> -->

        </div>
    </div>


@endsection
